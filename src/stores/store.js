
import { writable } from 'svelte/store'

const { set, update, subscribe } = writable("RAM KUMAR")
export default {
    subscribe,
    set,
    update,
    changeName: (name) => {
        setInterval(() => {
            let d = new Date()
            update(state => {
                return state =  d.toUTCString()
            })
        }, 1000)
    }
}