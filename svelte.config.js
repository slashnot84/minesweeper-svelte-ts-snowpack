const autoPreprocess = require('svelte-preprocess');

module.exports = {
  preprocess: autoPreprocess({
    defaults: {
      script: 'typescript',
    },
  }),
  // compilerOptions:{
  //   customElement: true
  // },
  // // compiler:{
  // //   customElement: true
  // // },
  // customElement: true
};